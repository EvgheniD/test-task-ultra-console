namespace DeliveryCli
{
    public class Delivery
    {
        public string uuid { get; set; }
        public string name { get; set; }
        public string uuid_product { get; set; }
        public string uuid_sp { get; set; }
        public DateTime create_datetime { get; set; }
        public bool delivered { get; set; }
    }
}