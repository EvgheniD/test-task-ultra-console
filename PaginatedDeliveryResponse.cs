namespace DeliveryCli
{
    public class PaginatedDeliveryResponse
    {
        public int PageNumber { get; set; }
        public int PageSize { get; set; }
        public int TotalCount { get; set; }
        public List<Delivery> data { get; set; }
    }
}