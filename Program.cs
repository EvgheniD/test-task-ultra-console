﻿namespace DeliveryCli
{
    using System;
    using System.Threading.Tasks;
    using System.Net.Http;

    class Program
    {
        static async Task Main(string[] args)
        {
            string serviceProvider = null;
            string jwtToken = null;
            bool write = false;
            bool read = false;

            // Parse command-line arguments
            for (int i = 0; i < args.Length; i++)
            {
                string arg = args[i];

                if (arg == "-sp" || arg == "--serviceprovider")
                {
                    // Parse service provider argument
                    if (i + 1 < args.Length)
                    {
                        serviceProvider = args[i + 1];
                        i++;
                    }
                    else
                    {
                        Console.WriteLine("Error: Service provider argument missing value");
                        return;
                    }
                }
                else if (arg == "-t" || arg == "--t")
                {
                    // Parse service provider argument
                    if (i + 1 < args.Length)
                    {
                        jwtToken = args[i + 1];
                        i++;
                    }
                    else
                    {
                        Console.WriteLine("Error: Service provider argument missing value");
                        return;
                    }
                }
                else if (arg == "-w" || arg == "--write")
                {
                    write = true;
                }
                else if (arg == "-r" || arg == "--read")
                {
                    read = true;
                }
                else
                {
                    Console.WriteLine($"Error: Unknown argument {arg}");
                    return;
                }
            }

            DeliveryReader deliveryReader = new DeliveryReader();
            DeliveryWriter deliveryWriter = new DeliveryWriter();
            
            if (jwtToken == null) 
            {
                Console.WriteLine("Error: Must specify -t flag with the JWT Token");
                return;
            }

            if ((!read && !write) || (read && write))
            {
                Console.WriteLine("Error: Must specify either -r or -w flag");
                return;
            }

            var client = new HttpClient();
            client.DefaultRequestHeaders.Add("Authorization", $"Bearer {jwtToken}");

            if (read)
            {
                var allDeliveries = new List<Delivery>();
                if (string.IsNullOrEmpty(serviceProvider))
                    allDeliveries = await deliveryReader.fetchDeliveriesAsync(false, client);
                else 
                    allDeliveries = await deliveryReader.fetchDeliveriesAsync(false, client, serviceProvider);
                Console.WriteLine($"Deliveries with delivery = {false}:");
                foreach (Delivery delivery in allDeliveries)
                {
                    Console.WriteLine($"Delivery: {delivery.name}, Product: {delivery.uuid_product}, Service Provider: {delivery.uuid_sp}, DeliveredStatus: {delivery.delivered}");
                }
            }

            if (write)
            {
                if (string.IsNullOrEmpty(serviceProvider))
                    await deliveryWriter.WriteToFileAndPatchOthersAsync(await deliveryReader.fetchDeliveriesAsync(false, client), client);
                else 
                    await deliveryWriter.WriteToFileAndPatchOthersAsync(await deliveryReader.fetchDeliveriesAsync(false, client, serviceProvider),client);
            }
        }
    }
}
