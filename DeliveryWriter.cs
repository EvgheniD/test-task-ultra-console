namespace DeliveryCli
{
    using System.Text;
    using System.Text.Json;

    public class DeliveryWriter
    {
        public DeliveryWriter() {}

        public async Task WriteToFileAndPatchOthersAsync(List<Delivery> deliveries, HttpClient client)
        {
                StringBuilder sb = new StringBuilder();

                foreach (var delivery in deliveries)
                {
                    sb.AppendLine($"Delivery ID: {delivery.uuid} Delivery Name: {delivery.name} ProductId: {delivery.uuid_product} DeliveryCreatedDate: {delivery.create_datetime}");
                    sb.AppendLine();
                }

                using (StreamWriter sw = new StreamWriter("deliveries.txt"))
                {
                    sw.Write(sb.ToString());
                }

                // Update delivery status on API
                foreach (Delivery delivery in deliveries)
                {
                    delivery.delivered = true;
                    var content = new StringContent(JsonSerializer.Serialize(delivery), Encoding.UTF8, "application/json");
                    var request = new HttpRequestMessage(HttpMethod.Patch, "https://localhost:7217/api/v1/deliveries")
                    {
                        Content = content
                    };
                    var response = await client.SendAsync(request);
                    if (!response.IsSuccessStatusCode)
                    {
                        Console.WriteLine($"Error: Failed to update delivery {delivery.uuid} on API: {response.StatusCode}");
                    }
                }
        }
    }
}