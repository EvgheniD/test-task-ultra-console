namespace DeliveryCli
{
    using System;
    using System.Collections.Generic;
    using System.Net.Http;
    using System.Net.Http.Json;
    using System.Threading.Tasks;
    public class DeliveryReader
    {
        public DeliveryReader() {}
        public async Task<List<Delivery>> fetchDeliveriesAsync(bool delvieryStatus, HttpClient client, string? serviceProviderName = null) 
        {
            const int PAGE_SIZE = 10;
            int pageNumber = 1;
            bool morePages = true;

            

            List<Delivery> allDeliveries = new List<Delivery>();

                while (morePages)
                {
                    // Build the URL with the current page number and page size
                    string url = $"https://localhost:7217/api/v1/deliveries?PageNumber={pageNumber}&PageSize={PAGE_SIZE}&DeliveryStatus={delvieryStatus}";

                    if (!string.IsNullOrEmpty(serviceProviderName))
                    {
                        url += $"&ServiceProviderName={serviceProviderName}";
                    }

                    // Send the HTTP GET request
                    HttpResponseMessage response = await client.GetAsync(url);

                    if (!response.IsSuccessStatusCode)
                    {
                        Console.WriteLine($"Error: Failed to fetch deliveries from API: {response.StatusCode}");
                        return allDeliveries;
                    }
                    // string json = await response.Content.ReadAsStringAsync(); 
                    // Console.WriteLine(json);
                    // Read the JSON response into an array of Delivery objects
                    var paginatedDeliveryResponse = await response.Content.ReadFromJsonAsync<PaginatedDeliveryResponse>();
                    var deliveries = paginatedDeliveryResponse.data;

                    if (deliveries == null)
                    {
                        Console.WriteLine("Error: Failed to parse delivery data");
                        return allDeliveries;
                    }

                    // Add the deliveries from the current page to the list of all deliveries
                    allDeliveries.AddRange(deliveries);

                    // Check if there are more pages
                    if (deliveries.Count < PAGE_SIZE)
                    {
                        morePages = false;
                    }
                    else
                    {
                        pageNumber++;
                    }
                }

          return allDeliveries;
        }
    }
}