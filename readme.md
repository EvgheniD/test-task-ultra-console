# DeliveryCli

DeliveryCli is a console application that allows users to read and write data from api, and write it to a file.

## Getting Started

The Console.exe file is located under the publishing folder. To use this application, you need to specify the service provider argument with the `-sp` or `--serviceprovider` flag, and the JWT Token with the `-t` or `--t` flag. You must also specify either the `-r` or `--read` flag to read data, or the `-w` or `--write` flag to write data. (the text file will appear in the same folder as .exe file) 

For example: 
```bash 
Console.exe -sp <service_provider> -t <jwt_token> -r  # To read data 
Console.exe -sp <service_provider> -t <jwt_token> -w  # To write data 
```